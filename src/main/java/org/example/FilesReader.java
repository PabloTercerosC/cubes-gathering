package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FilesReader {

    static List<String> friends = new ArrayList<>();
    static List<String[]> friendships = new ArrayList<>();

    public static void readFile(String path) {
        String line;

        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {

                if (!line.contains(" ")) {
                    friends.add(line);
                }
                else {
                    String[] edge = line.split(" ");
                    friendships.add(edge);
                }
            }

            bufferedReader.close();
        } catch (IOException ex) {
            System.out.println("Error reading file '" + path + "'");
        }
    }

    public static List<Edge> getGraph(String path) {
        readFile(path);
        List<Edge> graph = new ArrayList<>();

        for (String[] friendship : friendships) {
            graph.add(new Edge(friends.indexOf(friendship[0]), friends.indexOf(friendship[1]), Integer.parseInt(friendship[2])));
        }

        graph.sort(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return o2.weight - o1.weight;
            }
        });

        return graph;
    }

    public static List<String> getNames(String path) {
        friends.clear();
        readFile(path);

        return friends;
    }
}