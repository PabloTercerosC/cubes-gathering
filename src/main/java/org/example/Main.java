package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.example.Cube.recursion;

public class Main {
    public static void main(String[] args) {
        List<Edge> graphA = FilesReader.getGraph("C:/Users/Universidad/IdeaProjects/cubes-gathering/src/main/java/org/example/file1.txt");
        List<String> namesA = FilesReader.getNames("C:/Users/Universidad/IdeaProjects/cubes-gathering/src/main/java/org/example/file1.txt");
        int VA = 6;

        int mCaseA1 = 3;
        int mCaseA2 = 2;
        Cube.mGatherings(VA, mCaseA1, graphA, namesA);
        Cube.mGatherings(VA, mCaseA2, graphA, namesA);

        int xCaseA1 = 2;
        int xCaseA2 = 7;
        Cube.xRelationship(VA, xCaseA1, graphA, namesA);
        Cube.xRelationship(VA, xCaseA2, graphA, namesA);
    }
}