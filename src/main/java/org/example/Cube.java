package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cube {

	public static Subset[] kruskal(int V, int v, List<Edge> edges, boolean mCondition) {
		int j = 0;

		Subset[] subsets = new Subset[V];

		for (int i = 0; i < V; i++) {
			subsets[i] = new Subset(i, 0);
		}

		if (mCondition) {
			while (V > v) {
				Edge nextEdge = edges.get(j);
				int x = findRoot(subsets, nextEdge.src);
				int y = findRoot(subsets, nextEdge.dest);

				union(subsets, x, y);

				j++;
				V--;
			}
		}
		else {
			while (true) {
				Edge nextEdge = edges.get(j);
				if (nextEdge.weight > v) {
					int x = findRoot(subsets, nextEdge.src);
					int y = findRoot(subsets, nextEdge.dest);

					union(subsets, x, y);
					j++;
				}
				else {
					break;
				}
			}
		}

		return subsets;
	}

	public static void mGatherings(int V, int m, List<Edge> edges, List<String> names) {
		List<Subset> subsets = new ArrayList<>(Arrays.asList(kruskal(V, m, edges, true)));
		getResult(subsets, names);
	}

	public static void xRelationship(int V, int x, List<Edge> edges, List<String> names) {
		List<Subset> subsets = new ArrayList<>(Arrays.asList(kruskal(V, x, edges, false)));
		getResult(subsets, names);
	}

	private static void getResult(List<Subset> subsets, List<String> names) {
		List<Integer> result = new ArrayList<>();

		for (Subset subset : subsets) {
			if (subset != null) {
				recursion(subset, subsets, result);
				result.add(null);
			}
		}

		read(result, names);
		System.out.println();
	}

	private static void union(Subset[] subsets, int x, int y) {
		int rootX = findRoot(subsets, x);
		int rootY = findRoot(subsets, y);

		if (subsets[rootY].rank < subsets[rootX].rank) {
			subsets[rootY].parent = rootX;
		} else if (subsets[rootX].rank < subsets[rootY].rank) {
			subsets[rootX].parent = rootY;
		} else {
			subsets[rootY].parent = rootX;
			subsets[rootX].rank++;
		}
	}

	private static int findRoot(Subset[] subsets, int i) {
		if (subsets[i].parent == i)
			return subsets[i].parent;

		subsets[i].parent = findRoot(subsets, subsets[i].parent);
		return subsets[i].parent;
	}

	public static void recursion(Subset subset, List<Subset> subsets, List<Integer> result) {
		result.add(subsets.indexOf(subset));

		List<Integer> nodes = new ArrayList<>();
		for (int i = 0; i < subsets.size(); i++) {
			if (subsets.get(i) != null && subsets.get(i).equals(subset)) {
				continue;
			}
			if (subsets.get(i) != null && subsets.get(i).parent == subsets.indexOf(subset)) {
				nodes.add(i);
			}
		}

		subsets.set(subsets.indexOf(subset), null);

		for (Integer node : nodes) {
			if (subsets.get(node).rank > 0) {
				var a = subsets.get(node);
				recursion(a, subsets, result);
			} else {
				result.add(node);
			}
			subsets.set(node, null);
		}
	}

	public static void read(List<Integer> result, List<String> names) {

		for (Integer r : result) {
			if (r == null) {
				System.out.print("\n");
			} else {
				System.out.print(names.get(r) + " ");
			}
		}
	}
}